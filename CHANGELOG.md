# Changelog
All notable changes to this project will be documented in this file. See [conventional commits](https://www.conventionalcommits.org/) for commit guidelines.

- - -
## 0.0.26 - 2024-09-16
#### Miscellaneous Chores
- **(deps)** update dependency additi/external/gitlab-templates-common to v0.0.64 - (1bb840e) - dsi of2m.fr

- - -

## 0.0.25 - 2024-09-12
#### Miscellaneous Chores
- **(deps)** update dependency additi/external/gitlab-templates-common to v0.0.63 - (b1bc03c) - dsi of2m.fr

- - -

## 0.0.24 - 2024-08-28
#### Miscellaneous Chores
- **(deps)** update dependency additi/external/gitlab-templates-common to v0.0.61 - (006dac8) - dsi of2m.fr

- - -

## 0.0.23 - 2024-08-28
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (1be4d1b) - dsi.of2m.fr
#### Miscellaneous Chores
- **(deps)** update dependency additi/external/gitlab-templates-common to v0.0.59 - (7e4f525) - dsi of2m.fr

- - -

## 0.0.22 - 2024-08-27
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (103d5a2) - dsi.of2m.fr
#### Miscellaneous Chores
- **(deps)** update dependency additi/external/gitlab-templates-common to v0.0.52 - (1defb39) - youness.chetoui

- - -

## 0.0.21 - 2024-05-23
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (946baa6) - dsi.of2m.fr
#### Miscellaneous Chores
- **(deps)** update gcr.io/kubebuilder/kube-rbac-proxy docker tag to v0.16.0 - (b945554) - dsi of2m.fr
- **(deps)** update dependency additi/external/gitlab-templates-common to v0.0.34 - (81fb057) - dsi of2m.fr

- - -

## 0.0.20 - 2024-03-13
#### Features
- add sleepinfos custom resource definition - (9ea641e) - Arnaud Hatzenbuhler

- - -

## 0.0.19 - 2024-02-19
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (a973967) - dsi.of2m.fr
#### Miscellaneous Chores
- **(deps)** update dependency additi/external/gitlab-templates-common to v0.0.33 - (c94c61f) - dsi of2m.fr

- - -

## 0.0.18 - 2024-02-16
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (0219273) - dsi.of2m.fr
- **(README.md)** Update documentation [nopipeline] - (5b0d32b) - dsi.of2m.fr
#### Miscellaneous Chores
- **(deps)** update dependency additi/external/gitlab-templates-common to v0.0.26 - (d07ca21) - dsi of2m.fr
- **(deps)** pin dependencies - (5ff8dbd) - dsi of2m.fr

- - -

## 0.0.17 - 2024-02-16
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (d670ac1) - dsi.of2m.fr
#### Miscellaneous Chores
- Add support for replicaCount, priorityClassName and topologySpreadConstraints (Thanks @Riqardos for MR) - (6b2db6c) - youness.chetoui

- - -

## 0.0.16 - 2024-02-16
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (b8f0f07) - dsi.of2m.fr
#### Miscellaneous Chores
- **(renovate.json)** [nopipeline] [ci skip] change conf - (113e1c3) - youness.chetoui
- **(renovate.json)** add renovate conf - (1e6d520) - youness.chetoui
- **(values.yaml)** test renovate [nopipeline] - (d8bb22d) - youness.chetoui
- update values and renovate - (c09eab3) - youness.chetoui

- - -

## 0.0.15 - 2023-11-15
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (dd98466) - dsi.of2m.fr
#### Features
- **(Deployement.yaml)** :fire: Remove deprecated args logtostderr in kube-rbac-proxy container - (e1a09fd) - youness.chetoui
- **(values.yaml)** :arrow_up: Upgrade image kube-rbac-proxy to v0.15.0 - (eb8db13) - youness.chetoui
#### Miscellaneous Chores
- :bookmark: 0.0.11 - (14b14b9) - youness.chetoui

- - -

## 0.0.14 - 2023-11-15
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (a179160) - dsi.of2m.fr
- **(README.md)** Update documentation [nopipeline] - (ebb0c73) - dsi.of2m.fr
- **(README.md.gotmpl)** :memo: Modify readme template - (8fdb767) - youness.chetoui
#### Miscellaneous Chores
- **(.helmignore)** Add folder public in helmignore - (a5baea3) - youness.chetoui
- :bookmark: 0.0.10 - (d8474fb) - youness.chetoui

- - -

## 0.0.13 - 2023-11-15
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (f2b44e5) - dsi.of2m.fr
- **(README.mdgotmpl)** :memo: Add Prerequisite in readme template - (6841a9b) - youness.chetoui
#### Features
- **(Chart.yaml)** :sparkles: Add artifacthub.io license  icon - (98579c8) - youness.chetoui
#### Miscellaneous Chores
- **(.helmignore)** Add somes files in helmignore - (b75fd45) - youness.chetoui
- :bookmark: 0.0.9 - (40d6f6a) - youness.chetoui

- - -

## 0.0.12 - 2023-11-15
#### Miscellaneous Chores
- Modify repositoryID - (e96a7a9) - youness.chetoui

- - -

## 0.0.11 - 2023-11-15
#### Miscellaneous Chores
- **(artifacthub-repo.yml)** Ajout metadata file for artifact - (4d74fb1) - youness.chetoui

- - -

## 0.0.10 - 2023-11-15
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (947605f) - dsi.of2m.fr
- **(README.md.gotmpl)** :memo: Add sources section in readme template - (9b089eb) - youness.chetoui
#### Miscellaneous Chores
- :bookmark: 0.0.8 - (97fbc89) - youness.chetoui

- - -

## 0.0.9 - 2023-11-14
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (d254fd3) - dsi.of2m.fr
- **(README.md.gotmpl)** :bug: Fix name of chart in install instruction - (ba6faa7) - youness.chetoui

- - -

## 0.0.8 - 2023-11-14
#### Continuous Integration
- **(.gitlab-ci.yml)** :arrow_up: Ci 0.0.14 - (5a96e19) - youness.chetoui
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (fc5ce46) - dsi.of2m.fr
- :memo: Add readme.md.gotmpl - (4fd5ce7) - youness.chetoui
#### Miscellaneous Chores
- :bookmark: New version of chart - (321ff1c) - youness.chetoui

- - -

## 0.0.7 - 2023-10-31
#### Continuous Integration
- **(.gitlab-ci.yml)** Add template workflow - (0959efd) - youness.chetoui
- **(.gitlab-ci.yml)** Fix include file - (baf5f5b) - youness.chetoui
- **(.gitlab-ci.yml)** Update version of ci 0.0.11 -> 0.0.13 - (fe3560c) - youness.chetoui

- - -

## 0.0.6 - 2023-10-31
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (1851f69) - dsi.of2m.fr
#### Features
- **(webhook)** Enable or not webook - (9f238c2) - youness.chetoui
#### Miscellaneous Chores
- **(Chart.yaml)** New version 0.0.6 - (855ceaa) - youness.chetoui

- - -

## 0.0.5 - 2023-10-27
#### Bug Fixes
- **(Deployement.yaml)** Fix name of container proxy and indent - (c05d792) - youness.chetoui
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (6ce596d) - dsi.of2m.fr
#### Features
- **(Chart.yaml)** generate 0.0.4 version - (f710474) - youness.chetoui

- - -

## 0.0.4 - 2023-10-27
#### Bug Fixes
- **(index.yaml)** fix path of tgz in index.yaml - (021a0c0) - youness.chetoui
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (24c427f) - dsi.of2m.fr

- - -

## 0.0.3 - 2023-10-27
#### Bug Fixes
- **(Chart.yaml)** fix appVersion 0.5.1 -> 0.5.2 - (76a8deb) - youness.chetoui
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (857692a) - dsi.of2m.fr

- - -

## 0.0.2 - 2023-10-27
#### Continuous Integration
- **(.gitlab-ci.yml)** fix version number - (1f98651) - youness.chetoui
- **(.gitlab-ci.yml)** Fix version of ci - (81020c5) - youness.chetoui
- **(.gitlab-ci.yml)** fix stages - (2f07490) - youness.chetoui
- **(.gitlab-ci.yml)** test ci - (1cf310c) - youness.chetoui
- **(.gitlab-cu=i.yml)** fix stages - (77baaf9) - youness.chetoui
- fix needs on pages stages - (0457abb) - youness.chetoui
- Fix path of release template - (8812daf) - youness.chetoui
- test some ci modifications - (fb2c52f) - youness.chetoui
#### Miscellaneous Chores
- **(index.yaml)** fix urls of charts - (e745086) - youness.chetoui
- **(index.yaml)** Add index yaml for chart repository - (acab380) - youness.chetoui
- **(public)** add chart 0.0.1 - (3d40a61) - youness.chetoui

- - -

Changelog generated by [cocogitto](https://github.com/cocogitto/cocogitto).